import React, { Component } from 'react';
import styled from 'styled-components';

import { DEFAULT_PATH } from '../constants';
import Deal from './Deal';

const Container = styled.div`
  width: 75%;
  padding: 40px 50px; 

`

class DealList extends Component {
    constructor(props) {
      super(props);
  
      this._isMounted = false;
  
      this.state = {
        dealList: [],
        isLoading: false
      }
    }
  
    fetchDealListByStatus = (statusID) => {
      this.setState({ isLoading: true });

      const isMatchesStatus = ({ STAGE_ID, LEAD_ID }) => {
        return STAGE_ID === statusID && LEAD_ID === null;
      }
      
      fetch(`${DEFAULT_PATH}/crm.deal.list/`)
        .then(res => res.json())
        .then(({ result: dealList }) => dealList.filter(isMatchesStatus))
        .then(dealList => {
          this._isMounted && this.setState({ dealList, isLoading: false })
        })
        .catch(error => this.setState({ error }))
    }
  
    componentWillReceiveProps(nextProps) {
      if (nextProps.statusID !== this.props.statusID) {
        this.setState({dealList: []});
        this.fetchDealListByStatus(nextProps.statusID);
      }
    }
  
    componentDidMount() {
      this._isMounted = true;
    }
  
    componentWillUnmount() {
      this._isMounted = false;
    }
  
    render() {
      const { dealList, isLoading } = this.state;
      return (
        <Container>
          <h2>Сделки: </h2>
          {
            isLoading
            ? <p>Загрузка...</p>
            : (dealList.length === 0
              ? <p>Сделок нет</p>
              : dealList.map(deal => 
                  <Deal key={deal.ID} deal={deal}/>
                )
            )
          }
          {console.log(dealList)}
        </Container>
      );
    }
  }

  export default DealList;