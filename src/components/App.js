import React, { Component } from 'react';
import styled from 'styled-components';

import StatusList from './StatusList';
import DealList from './DealList';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 1920px;
  min-width: 1280px;
  min-height: 100vh;
  margin: 0 auto;
  font-family: 'Roboto', sans-serif;
  background-color: #F5F6FA;
  color: #000000;
`

const Row = styled.div`
  display: flex;
`

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      statusSelectedID: null
    };
  }
  
  onSelectStatus = (statusID) => {
    this.setStutusSelectedID(statusID)
  }

  setStutusSelectedID = (statusID) => {
    this.setState({ statusSelectedID: statusID });
  }

  render() {
    const {
      statusSelectedID,
    } = this.state;

    return (
      <Container>
        <Row>
          <StatusList
            setStutusSelectedID={this.setStutusSelectedID}
            statusSelectedID={statusSelectedID} 
            onSelectStatus={this.onSelectStatus}
          />
          <DealList
            statusID={statusSelectedID}
          />
        </Row>
      </Container>
    );
  }
}

export default App;
