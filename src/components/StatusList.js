import React, { Component } from 'react';
import styled from 'styled-components';

import { DEFAULT_PATH } from '../constants';

import logoFilter from '../filter.svg';

const Container = styled.div`
  width: 25%;
  min-height: 100vh;
  font-size: 18px;
  background-color: #FAFBFD;
`

const Title = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 210px;
  margin: 50px auto 25px auto;
  padding: 20px 15%;
  color: #FFFFFF;
  background-color: #4D5AFF;
  border-radius: 50px;
`

const List = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
`
const Link = styled.a`
  display: block;
  padding: 20px 25px;
  text-decoration: none;
  font-weight: 700;
  color: ${
    props =>
      (props.statusID === props.statusSelectedID
      && '#ffffff') || '#000000'
  };
  background-color: ${
    props =>
      (props.statusID === props.statusSelectedID
      && '#4D5AFF') || 'inherit'
  };
  :hover {
    background-color: #4D5AFF;
    color: #FFFFFF;
  }
`



class StatusList extends Component {
  constructor(props) {
    super(props);

    this._isMounted = false;
    this.state = {
      statusList: [],
      error: null
    }
  }

  fetchStatusList = () => {
    const isStatusDeal = (status) => 
      status.ENTITY_ID === 'DEAL_STAGE' 

    fetch(`${DEFAULT_PATH}/crm.status.list/`)
      .then(res => res.json())
      .then(({ result: statusList }) => statusList.filter(isStatusDeal))
      .then(statusList => {
        if (this._isMounted) {
          this.setState({ statusList });
  
          const firstStatusID = statusList[0].STATUS_ID;
          this.props.setStutusSelectedID(firstStatusID);
        }
      })
      .catch(error => this.setState({ error }))
  }

  componentDidMount = () => {
    this._isMounted = true;
    this.fetchStatusList();
  }

  componentWillUnmount = () => {
    this._isMounted = false;
  }

  render() {
    const { statusList } = this.state;
    const { statusSelectedID, onSelectStatus } = this.props;

    return (
      <Container>
        <Title>
          <img src={logoFilter} alt="logo filter"/> 
          <span>Стадии</span>
        </Title>
        <List>
          { statusList.map(({ STATUS_ID, NAME }) => 
              <li key={STATUS_ID}>
                <Link href="#"
                  statusID={STATUS_ID}
                  statusSelectedID={statusSelectedID}
                  onClick={() => onSelectStatus(STATUS_ID)}
                >
                  {NAME}
                </Link>
              </li>
          )
          }
        </List>
      </Container>
    );
  }
}

export default StatusList;