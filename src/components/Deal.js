import React, { Component } from 'react';
import styled from 'styled-components';

import { DEFAULT_PATH } from '../constants';

const Container = styled.div`
  min-height: 200px;
  width: 80%;
  background-color: #ffffff;
  border-radius: 10px;
`

const Title = styled.div`
  padding: 10px 10px;
  font-weight: 700;
  border-bottom: 3px solid #FAFBFD;
`

const DealDitails = styled.div`
  display: flex;
  padding: 10px;
`

const Sum = styled.div`
  width: 50%;
`

const ProductList = styled.div`
  width: 50%;
`

class Deal extends Component {
    constructor(props) {
      super(props);
      
      this._isMounted = false;
      this.state = {
        productList: [],
        error: null
      }
    }
    
    fetchProductListByDeal = (dealID) => {
      fetch((`${DEFAULT_PATH}/crm.productrow.list.json/?filter[OWNER_TYPE]=d&filter[OWNER_ID]=${dealID}`))
        .then(res => res.json())
        .then(({ result: productList }) => {
          this._isMounted && this.setState({ productList })
        })
        .catch(error => this.setState({ error }))
    }

    componentWillUnmount() {
      this._isMounted = false;
    }
  
    componentDidMount() {
      this._isMounted = true;
      const {ID} = this.props.deal;
      this.fetchProductListByDeal(ID)
    }
  
    render() {
      const { productList } = this.state; 
      const { deal } = this.props;

      return (
        <Container>
          <Title>{deal.TITLE}</Title>
          <DealDitails>
            <Sum>
                <span>Сумма: {parseFloat(deal.OPPORTUNITY).toLocaleString()} тг.</span>
            </Sum>
            <ProductList>
              Перечень товаров:
              {
                productList.map(product => 
                  <div key={product.ID} className="product-item">
                    <p> {product.PRODUCT_NAME} | {parseFloat(product.QUANTITY)} шт. | {parseFloat(product.PRICE).toLocaleString()} тг.</p>
                  </div>
                )
              }
            </ProductList>
          </DealDitails>
        </Container>
      );
    }
}

export default Deal;